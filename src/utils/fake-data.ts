import {faker} from '@faker-js/faker';

import {IListItem} from '../screens/list';

const list_data: IListItem[] = [];

for (let index = 0; index < 1500; index++) {
  const id = faker.datatype.uuid();
  const name = faker.commerce.productName();
  const description = faker.commerce.productDescription();
  const price = faker.commerce.price();
  const priceInt = parseFloat(faker.commerce.price());
  const salePrice = faker.helpers.maybe(
    () => faker.commerce.price(priceInt * 0.5, priceInt * 0.9),
    {probability: 0.1},
  );
  const brand = faker.company.name();

  list_data.push({
    id,
    name,
    description,
    price,
    salePrice,
    brand,
    index,
  });
}

export default list_data;
