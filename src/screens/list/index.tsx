import React, {useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import ListData from '../../utils/fake-data';
import {ListItem} from './components/item';
import {FlashList} from '@shopify/flash-list';
import {StyleSheet} from 'react-native';
export interface IListItem {
  id: string;
  name: string;
  description: string;
  price: string;
  salePrice: any;
  brand: string;
  index: number;
}

const ListScreen = () => {
  const renderItem = useCallback(
    ({item}: {item: IListItem}) => <ListItem item={item} />,
    [],
  );

  return (
    <SafeAreaView style={styles.container} edges={['bottom']}>
      <FlashList
        testID="list"
        data={ListData}
        contentContainerStyle={styles.contentContainer}
        keyExtractor={item => item.id}
        renderItem={renderItem}
        // https://shopify.github.io/flash-list/docs/estimated-item-size/
        estimatedItemSize={90}
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  contentContainer: {
    paddingHorizontal: 16,
  },
});

export default ListScreen;
