import React, {Dispatch, SetStateAction} from 'react';
import styled from '@emotion/native';

import {Typography} from './typography';
import {StyleSheet} from 'react-native';

export const CartQuantity: React.FC<{
  quantity: number;
  update: Dispatch<SetStateAction<number>>;
}> = ({quantity, update}) => {
  return (
    <QuantityContainer>
      <React.Fragment>
        <QuantityButton
          onPress={() => update(quantity + 1)}
          underlayColor="#EDEBF2">
          <Typography color="#522973">+</Typography>
        </QuantityButton>

        <Typography style={styles.quantity}>{quantity}</Typography>

        <QuantityButton
          disabled={quantity === 1}
          onPress={() => update(quantity - 1)}
          underlayColor="#EDEBF2">
          <Typography color="#522973">-</Typography>
        </QuantityButton>
      </React.Fragment>
    </QuantityContainer>
  );
};

const styles = StyleSheet.create({
  quantity: {
    textAlign: 'center',
    flex: 1,
  },
});

const QuantityButton = styled.TouchableHighlight({
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 100,
  width: 40,
  height: 40,
});

const QuantityContainer = styled.TouchableHighlight({
  borderWidth: 1,
  borderColor: '#EDEBF2',
  marginRight: 10,
  flex: 4,
  paddingVertical: 0,
  paddingHorizontal: 5,
  borderRadius: 1000,
  maxHeight: 51,
  alignItems: 'center',
  flexDirection: 'row',
});
