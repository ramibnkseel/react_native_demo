import React from 'react';
import 'react-native';
import {RenderAPI, render} from '@testing-library/react-native';
import {ListItem} from '../src/screens/list/components/item';
import ListScreen from '../src/screens/list';

describe('ListScreen', () => {
  it('renders the list', () => {
    const {getAllByTestId}: RenderAPI = render(<ListScreen />);
    const listItem = getAllByTestId('list');
    expect(listItem.length).toBeGreaterThan(0);
  });
});

/* eslint-disable no-undef */
jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: jest.fn(),
    useRoute: jest.fn(),
  };
});

describe('List Items', () => {
  it('renders the list items', () => {
    const mockItem = {
      id: 'rijijg',
      name: 'Mock Item',
      description: 'Mock Item Description',
      price: '100',
      salePrice: '90',
      brand: 'Mock Brand',
      index: 1,
    }; // Mock item object
    const {getAllByTestId}: RenderAPI = render(<ListItem item={mockItem} />);
    const listItem = getAllByTestId('item-1');
    expect(listItem.length).toBeGreaterThan(0);
  });
});
